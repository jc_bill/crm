package com.cheng.crm.web.interceptor;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.util.Constants;
import com.cheng.crm.util.UserContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        System.out.println("LoginInterceptor.preHandle");
        Employee employee = (Employee) request.getSession().getAttribute(Constants.USER_IN_SESSION);
        //设置请求到当前的线程中
        UserContext.setLocalThread(request);
        if (employee == null) {
            response.sendRedirect("/login.jsp");
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
