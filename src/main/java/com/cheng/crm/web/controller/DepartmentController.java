package com.cheng.crm.web.controller;

import com.cheng.crm.domain.Department;
import com.cheng.crm.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@Scope("prototype")
public class DepartmentController {

    @Autowired
    private IDepartmentService service;

    @RequestMapping(value = "department_list")
    @ResponseBody
    public List getList(){
        List result = service.list();
        return result;
    }
}
