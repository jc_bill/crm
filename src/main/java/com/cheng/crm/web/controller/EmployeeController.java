package com.cheng.crm.web.controller;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.EmployeeQueryObject;
import com.cheng.crm.service.IEmployeeService;
import com.cheng.crm.util.Constants;
import com.cheng.crm.util.UserContext;
import com.cheng.crm.vo.EmployeeResult;
import com.cheng.crm.vo.LoginResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
@Scope("prototype")
@SuppressWarnings("all")
public class EmployeeController {

    @Autowired
    private IEmployeeService service;

    @RequestMapping("/main")
    public String index(){
        return "index";
    }

    @RequestMapping("employee")
    public String employeeContent(){
        return "employee";
    }

    /*
    * @ModelAttribute加了这个标签，beforeMethod在每个方法运行前运行
    * 解决update方法的部分字段数据丢失
    * 1、先把更新的数据查询出来
    * 2、把数据设置进update
    * 3、前端jsp部分数据覆盖数据(处理前端数据)
    * */
    @ModelAttribute
    public void beforeMethod(Long id, Model model)
    {
        if (id != null) {
            Employee employee = service.getById(id);
            model.addAttribute(employee);
        }
    }

    @RequestMapping(value = "/employee_save",method = RequestMethod.POST)
    @ResponseBody
    public EmployeeResult save(Employee employee){
        EmployeeResult result = new EmployeeResult();
        try {
            employee.setInputtime(new Date());
            int effectCount = service.save(employee);
            if (effectCount > 0) {
                result.setSuccess(true);
                result.setMsg("保存成功");
            }else{
                result.setSuccess(false);
                result.setMsg("保存失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMsg("发生异常，请联系管理员");
        }
        return result;
    }

    @RequestMapping(value = "/employee_delete",method = RequestMethod.GET)
    @ResponseBody
    public EmployeeResult delete(Long id){
        EmployeeResult result = new EmployeeResult();
        try {
            int effectCount = service.delete(id);
            if (effectCount > 0) {
                result.setSuccess(true);
                result.setMsg("删除成功");
            }else{
                result.setSuccess(false);
                result.setMsg("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMsg("发生异常，请联系管理员");
        }
        return result;
    }

    @RequestMapping(value = "/employee_update",method = RequestMethod.POST)
    @ResponseBody
    public EmployeeResult update(Employee employee){
        EmployeeResult result = new EmployeeResult();
        if (employee != null) {
            try {
                int effectCount = service.update(employee);
                if (effectCount > 0) {
                    result.setSuccess(true);
                    result.setMsg("更新成功");
                }else{
                    result.setSuccess(false);
                    result.setMsg("更新失败");
                }
            } catch (Exception e) {
                e.printStackTrace();
                result.setSuccess(false);
                result.setMsg("发生异常，请联系管理员");
            }
        }
        return result;
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public LoginResult login(String username, String password, HttpServletRequest request){
        //这个必须在service.getLoginMessage(username, password)，之前，因为当service.getLoginMessage 之后，会马上执行LogUtil.writeLog
        //这个时候，还没设置UserContext.setLocalThread(request);，会导致空指针
        UserContext.setLocalThread(request);
        Employee loginMessage = service.getLoginMessage(username, password);
        LoginResult loginResult = new LoginResult();
        if (loginMessage == null) {
            loginResult.setSuccess(false);
            loginResult.setMsg("用户不存在或者密码错误");
        }else{
            request.getSession().setAttribute(Constants.USER_IN_SESSION,loginMessage);
            loginResult.setSuccess(true);
            loginResult.setMsg("登录成功");
        }
        return loginResult;
    }

    @RequestMapping("/employee_list")
    @ResponseBody
    public PageResult list(EmployeeQueryObject queryObject){
        return service.getListByCondition(queryObject);
    }
}
