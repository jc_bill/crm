package com.cheng.crm.web.controller;

import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.PermissionQueryObject;
import com.cheng.crm.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Scope("prototype")
public class PermissionController {
    @Autowired
    private IPermissionService permissionService;

    @RequestMapping("/permission_list")
    @ResponseBody
    public PageResult list(PermissionQueryObject queryObject){
        PageResult pageResult = permissionService.getListByCondition(queryObject);
        return pageResult;
    }
}
