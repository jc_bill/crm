package com.cheng.crm.util;

import javax.servlet.http.HttpServletRequest;

public class UserContext {

    //创建当前线程对象
    private static ThreadLocal<HttpServletRequest> thread = new ThreadLocal<>();

    public static void setLocalThread(HttpServletRequest request){
        thread.set(request);
    }

    public static HttpServletRequest getLocalThread(){
        return thread.get();
    }

}
