package com.cheng.crm.mapper;

import com.cheng.crm.domain.Permission;
import com.cheng.crm.query.PermissionQueryObject;

import java.util.List;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long id);
    int insert(Permission record);
    Permission selectByPrimaryKey(Long id);
    List<Permission> selectAll();
    int updateByPrimaryKey(Permission record);
    Long selectByCount(PermissionQueryObject queryObject);
    List<Permission> selectByCondition(PermissionQueryObject queryObject);
}