package com.cheng.crm.mapper;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.query.EmployeeQueryObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    int deleteByPrimaryKey(Long id);
    int insert(Employee record);
    Employee selectByPrimaryKey(Long id);
    List<Employee> selectAll();
    int updateByPrimaryKey(Employee record);
    Long selectByCount(EmployeeQueryObject queryObject);
    List<Employee> selectByCondition(EmployeeQueryObject queryObject);
    Employee selectByLogin(@Param("username")String username,@Param("password")String password);
}