package com.cheng.crm.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResultObject {
    protected boolean success;
    protected String msg;
}
