package com.cheng.crm.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class SystemLog {
    private Long id;
    private Employee user;
    private Date optime;
    private String opip;
    private String function;
    private String params;
}