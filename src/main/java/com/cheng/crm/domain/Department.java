package com.cheng.crm.domain;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

@Setter
@Getter
@Alias("Department")
public class Department{
    private Long id;
    private String sn;
    private String name;
    private Employee manager;
    private Department parent;
    private Boolean state;
}