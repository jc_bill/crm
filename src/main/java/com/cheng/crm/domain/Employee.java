package com.cheng.crm.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Setter
@Getter
@Alias("Employee")
public class Employee {
    private Long id;
    private String username;
    private String realname;
    private String password;
    private String tel;
    private String email;
    private Department dept;
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT-8")
    private Date inputtime;
    private Boolean state = true;
    private Boolean admin;
}