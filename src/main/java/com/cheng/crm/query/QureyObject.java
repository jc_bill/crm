package com.cheng.crm.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QureyObject {
    public Long page;//第几页
    public Long rows;//每页的大小
    protected Long start;//第几条开始分页

    //开始的条数
    public Long getStart(){
        return this.start = (this.page - 1) * rows;
    }
}
