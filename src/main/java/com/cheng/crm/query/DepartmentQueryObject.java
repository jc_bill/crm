package com.cheng.crm.query;

import lombok.Getter;
import lombok.Setter;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Setter
@Getter
@Alias("DepartmentQueryObject")
public class DepartmentQueryObject extends QureyObject{
    private String keyword;
    private Date startDate;
    private Date endDate;
}
