package com.cheng.crm.service;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.EmployeeQueryObject;

import java.util.List;

public interface IEmployeeService {
    int save(Employee employee);
    int delete(Long id);
    int update(Employee employee);
    Employee getById(Long id);
    Long getCount(EmployeeQueryObject queryObject);
    List<Employee> list();
    PageResult getListByCondition(EmployeeQueryObject queryObject);
    Employee getLoginMessage(String username,String password);
}
