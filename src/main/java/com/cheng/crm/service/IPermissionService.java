package com.cheng.crm.service;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.domain.Permission;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.EmployeeQueryObject;
import com.cheng.crm.query.PermissionQueryObject;

import java.util.List;

public interface IPermissionService {
    int save(Permission permission);
    int delete(Long id);
    int update(Permission permission);
    Permission getById(Long id);
    Long getCount(PermissionQueryObject queryObject);
    PageResult getListByCondition(PermissionQueryObject queryObject);
}
