package com.cheng.crm.service;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.domain.SystemLog;

public interface ISystemLogService {
    int save(SystemLog systemLog);
}
