package com.cheng.crm.service.impl;

import com.cheng.crm.domain.Permission;
import com.cheng.crm.mapper.PermissionMapper;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.PermissionQueryObject;
import com.cheng.crm.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements IPermissionService{

    @Autowired
    private PermissionMapper dao;

    @Override
    public int save(Permission permission) {
        return dao.insert(permission);
    }

    @Override
    public int delete(Long id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Permission permission) {
        return dao.updateByPrimaryKey(permission);
    }

    @Override
    public Permission getById(Long id) {
        return dao.selectByPrimaryKey(id);
    }

    @Override
    public Long getCount(PermissionQueryObject queryObject) {
        return dao.selectByCount(queryObject);
    }

    @Override
    public PageResult getListByCondition(PermissionQueryObject queryObject) {
        Long total = this.getCount(queryObject);
        PageResult pageResult = null;
        if(total == 0){
            pageResult = PageResult.empty;
        }else{
            pageResult = new PageResult(total,dao.selectByCondition(queryObject));
        }
        return pageResult;
    }
}
