package com.cheng.crm.service.impl;

import com.cheng.crm.domain.SystemLog;
import com.cheng.crm.mapper.SystemLogMapper;
import com.cheng.crm.service.ISystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemLogServiceImpl implements ISystemLogService {

    @Autowired
    private SystemLogMapper dao;

    @Override
    public int save(SystemLog systemLog) {
        return dao.insert(systemLog);
    }
}
