package com.cheng.crm.service.impl;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.mapper.EmployeeMapper;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.EmployeeQueryObject;
import com.cheng.crm.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService{

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public int save(Employee employee) {
        return employeeMapper.insert(employee);
    }

    @Override
    public int delete(Long id) {
        return employeeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int update(Employee employee) {
        return employeeMapper.updateByPrimaryKey(employee);
    }

    @Override
    public Employee getById(Long id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    @Override
    public Long getCount(EmployeeQueryObject queryObject) {
        return employeeMapper.selectByCount(queryObject);
    }

    @Override
    public PageResult getListByCondition(EmployeeQueryObject queryObject) {
        Long total = this.getCount(queryObject);
        PageResult pageResult = null;
        if(total == 0){
            pageResult = PageResult.empty;
        }else{
            pageResult = new PageResult(total,employeeMapper.selectByCondition(queryObject));
        }
        return pageResult;
    }

    @Override
    public List<Employee> list() {
        return employeeMapper.selectAll();
    }

    @Override
    public Employee getLoginMessage(String username, String password) {
        return employeeMapper.selectByLogin(username,password);
    }
}
