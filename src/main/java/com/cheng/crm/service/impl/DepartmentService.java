package com.cheng.crm.service.impl;

import com.cheng.crm.domain.Department;
import com.cheng.crm.mapper.DepartmentMapper;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.DepartmentQueryObject;
import com.cheng.crm.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService implements IDepartmentService{

    @Autowired
    private DepartmentMapper mapper;

    @Override
    public void save(Department department) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void update(Department employee) {

    }

    @Override
    public Long getCount(DepartmentQueryObject queryObject) {
        return null;
    }

    @Override
    public List<Department> list() {
        return mapper.selectAll();
    }

    @Override
    public PageResult getListByCondition(DepartmentQueryObject queryObject) {
        return null;
    }
}
