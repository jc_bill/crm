package com.cheng.crm.service;

import com.cheng.crm.domain.Department;
import com.cheng.crm.domain.Employee;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.DepartmentQueryObject;
import com.cheng.crm.query.EmployeeQueryObject;

import java.util.List;

public interface IDepartmentService {
    void save(Department department);
    void delete(Long id);
    void update(Department employee);
    Long getCount(DepartmentQueryObject queryObject);
    List<Department> list();
    PageResult getListByCondition(DepartmentQueryObject queryObject);
}
