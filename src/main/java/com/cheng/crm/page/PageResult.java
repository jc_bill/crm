package com.cheng.crm.page;

import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

@Setter
@Getter
public class PageResult {
    private Long total;//总数
    private List rows;//结果集
    public static final PageResult empty = new PageResult(0L, Collections.emptyList());

    public PageResult(Long total, List rows) {
        this.total = total;
        this.rows = rows;
    }
}
