<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <jsp:include page="/js/linkHead.jsp"></jsp:include>
    <script type="text/javascript" src="/js/jquery-easyui-1.5.2/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="/js/views/role.js"></script>
</head>
<body class="easyui-layout" border="false">
<table id="datagrid" class="easyui-datagrid" title="Multiple Sorting" toolbar='#tb'>
</table>
<div id="tb">
    <div id="linkDiv">
        <a class="easyui-linkbutton" iconCls="icon-add" plain="true" data-cmd="add">新增</a>
        <a class="easyui-linkbutton" iconCls="icon-remove" plain="true" data-cmd="delete">离职</a>
        <a class="easyui-linkbutton" iconCls="icon-edit" plain="true" data-cmd="edit">修改</a>
        <a class="easyui-linkbutton" iconCls="icon-reload" plain="true" data-cmd="refresh">刷新</a>
    </div>
    <form id="role_search_form">
        <div>
            关键字：<input class="easyui-textbox" name="keyword" style="width:150px">
            日期：<input class="easyui-datebox" name="startDate"/> -
            <input class="easyui-datebox" name="endDate"/>
            状态：<select name="state">
                    <option value="">全部</option>
                    <option value="1">在职</option>
                    <option value="0">离职</option>
                  </select>
            <a id="searchBtn" class="easyui-linkbutton" iconCls="icon-search" plain="true" data-cmd="search">搜索</a>
        </div>
    </form>
</div>
<!-- 修改新增对话框 -->
<div id="role_dialog" class="easyui-dialog" data-options="buttons:'#bt',closed:false,modal:true" style="padding-left: 35px;padding-top: 50px">
    <form id="role_dialog_form" action="#" method="post" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td>角色编号:<input name="sn" type="text"> &nbsp;&nbsp;</td>
                <td>角色名称:<input name="username" type="text"></td>
            </tr>
           <tr>
               <td><table id="selfPermission"></table></td>
               <td><table id="allPermission"></table></td>
           </tr>
        </table>
    </form>
</div>

<div id="bt" align="center">
    <a href="#" class="easyui-linkbutton" data-options="plain:true" iconCls="icon-ok" data-cmd="submit">确定</a>
    <a href="#" class="easyui-linkbutton" data-options="plain:true" iconCls="icon-cancel" data-cmd="cancel">取消</a>
</div>
</body>
</html>