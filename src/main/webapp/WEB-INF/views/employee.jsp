<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <jsp:include page="/js/linkHead.jsp"></jsp:include>
    <script type="text/javascript" src="/js/jquery-easyui-1.5.2/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="/js/views/employee.js"></script>
    <style type="text/css">
        .datagrid-row {
            height: 30px;
        }
    </style>
</head>
<body class="easyui-layout" border="false">
<table id="datagrid" class="easyui-datagrid" title="Multiple Sorting" toolbar='#tb'>
    <%--<thead>--%>
    <%--<tr style=".datagrid-row">--%>
        <%--<!--halign:'center'表头居中，sortable:是否支持排序,formatter格式化处理,align:'center'数据居中-->--%>
        <%--<th data-options="field:'id',width:100" align="center">id</th>--%>
        <%--<th data-options="field:'username',width:100" align="center">username</th>--%>
        <%--<th data-options="field:'realname',width:100" align="center">realname</th>--%>
        <%--<th data-options="field:'password',width:100" align="center">password</th>--%>
        <%--<th data-options="field:'tel',width:100" align="center">tel</th>--%>
        <%--<th data-options="field:'email',width:100" align="center">email</th>--%>
        <%--<th data-options="field:'dept',width:100" align="center" formatter="deptFormatter">dept</th>--%>
        <%--<th data-options="field:'inputtime',width:100" align="center">inputtime</th>--%>
        <%--<th data-options="field:'state',width:100" align="center" formatter="stateFormatter">state</th>--%>
        <%--<th data-options="field:'admin',width:100" align="center" formatter="adminFormatter">admin</th>--%>
    <%--</tr>--%>
    <%--</thead>--%>
</table>
<div id="tb">
    <div id="linkDiv">
        <a class="easyui-linkbutton" iconCls="icon-add" plain="true" data-cmd="add">新增</a>
        <a class="easyui-linkbutton" iconCls="icon-remove" plain="true" data-cmd="delete">离职</a>
        <a class="easyui-linkbutton" iconCls="icon-edit" plain="true" data-cmd="edit">修改</a>
        <a class="easyui-linkbutton" iconCls="icon-reload" plain="true" data-cmd="refresh">刷新</a>
    </div>
    <form id="employee_search_form">
        <div>
            关键字：<input class="easyui-textbox" name="keyword" style="width:150px">
            日期：<input class="easyui-datebox" name="startDate"/> -
            <input class="easyui-datebox" name="endDate"/>
            状态：<select name="state">
                    <option value="">全部</option>
                    <option value="1">在职</option>
                    <option value="0">离职</option>
                  </select>
            <a id="searchBtn" class="easyui-linkbutton" iconCls="icon-search" plain="true" data-cmd="search">搜索</a>
        </div>
    </form>
</div>
<!-- 修改新增对话框 -->
<div id="employee_dialog" class="easyui-dialog" data-options="buttons:'#bt',closed:true,modal:true"
     style="width:400px;padding:30px 60px">
    <form id="employee_dialog_form" action="#" method="post" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td>username</td>
                <td><input type="text" name="username"></td>
            </tr>
            <tr>
                <td>realname</td>
                <td><input type="text" name="realname"></td>
            </tr>
            <tr>
                <td>password</td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td>tel</td>
                <td><input type="text" name="tel"></td>
            </tr>
            <tr>
                <td>部门</td>
                <td><input id="dept_combobox" class="easyui-combobox" name="dept.id" data-options="url:'department_list',method:'get',valueField:'id',
                                  textField:'name',panelHeight:'auto'">
                </td>
            </tr>
            <tr>
                <td>email</td>
                <td><input type="text" name="email"></td>
            </tr>
        </table>
    </form>
</div>

<div id="bt" align="center">
    <a href="#" class="easyui-linkbutton" data-options="plain:true" iconCls="icon-ok" data-cmd="submit">确定</a>
    <a href="#" class="easyui-linkbutton" data-options="plain:true" iconCls="icon-cancel" data-cmd="cancel">取消</a>
</div>
</body>
</html>