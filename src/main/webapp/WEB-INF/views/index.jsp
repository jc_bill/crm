<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/js/linkHead.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/js/jquery-easyui-1.5.2/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="/js/views/index.js"></script>
    <style type="text/css">
        <!--设置每行的高度-->
        .center {
            margin-right: auto;
            margin-left: auto;
        }
    </style>
</head>
<body class="easyui-layout"  border="false">
<div class="easyui-layout" data-options="fit:true" style="width: 600px;height: 400px;">
    <!-- no-repeat不重复，cover：大小为覆盖整个layout -->
    <div data-options="region:'north'" style="height: 100px;background: url('/img/banner-pic.gif') no-repeat;background-size: cover">
        <div  align="center" style="padding-top: 10px"><font color="blue" size="30px">晓勇哥lol黄金系统</font></div>
    </div>
    <div data-options="region:'west',title:'west',border:false" style="width: 250px">
        <!-- fit:true 填充整个页面，给人家拉风琴的效果-->
        <div class="easyui-accordion" data-options="fit:true">
            <div title="菜单" iconCls="icon-sum">
                <div id="tree"></div>
            </div>
            <div title="简介" iconCls="icon-tip"></div>
            <div title="帮助" iconCls="icon-help"></div>
        </div>
    </div>
    <div data-options="region:'center',title:'center'" style="padding: 5px">
        <div id="mytabs" class="easyui-tabs" data-options="fit:true">
            <div title="欢迎页" iconCls="icon-tip" closable="true" style="padding: 10px;">欢迎使用晓勇哥系统</div>
        </div>
    </div>
    <div data-options="region:'south'" style="height: 100px;background: url('/img/banner-pic.gif') no-repeat;background-size: cover">
        <div align="center" style="padding-top: 45px">版权归晓勇哥所有</div>
    </div>
</div>
</body>
</html>