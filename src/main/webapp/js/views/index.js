$(function () {
    $("#tree").tree({
        url: '/data/tree.json',
        animate: true,//动画
        lines: true,//树线
        //每个子树的数量
        formatter:function (node) {
            if(node.children){
                var len = node.children.length;
                return node.text + "<font color='red'>("+len+")</font>"
            }
            return node.text;
        },
        //双击展开或折叠
        onDblClick:function (node) {
            var state = node.state;
            if(state=="open"){
                $("#tree").tree("collapse",node.target);
            }else{
                $("#tree").tree("expand",node.target);
            }
        },
        onClick:function (node) {
            if(node.attributes){
                if($("#mytabs").tabs("exists",node.text)){
                    //已经存在
                    $("#mytabs").tabs("select",node.text);
                }else {
                    $("#mytabs").tabs("add",{
                        title:node.text,
                        closable:true,
                        // href:node.attributes.url不适用
                        //frameborder内陷线为0
                        content:'<iframe src="'+node.attributes.url+'" style="width: 100%;height: 100%" frameborder="0"></iframe>'
                    });
                }
                // var url = node.attributes.url;
                console.info(node);
            }
        }
    });
});