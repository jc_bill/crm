$(function () {
    var roleDatagrid,roleDialog,roleForm,roleSearch,selfPermissionDatagrid,allPermissionDatagrid;
    roleDatagrid = $("#datagrid");
    roleDialog = $("#role_dialog");
    roleForm = $("#role_dialog_form");
    roleSearch = $("#role_search_form");
    selfPermissionDatagrid = $("#selfPermission");
    allPermissionDatagrid = $("#allPermission");

    roleDatagrid.datagrid({
        singleSelect:true,
        collapsible:true,
        url:"/employee_list",
        method:"post",
        pagination:true,
        pagePosition:"bottom",
        pageList: [3,5,10],
        pageSize:10,
        rownumbers:true,
        fit:true,
        fitColumns:true,
        onClickRow:function (rowIndex,rowData) {
            if(rowData.state){
                //eq 获取对应index的控件
                $("#linkDiv a").eq(1).linkbutton('enable');
                $("#linkDiv a").eq(2).linkbutton('enable')
            }else{
                $("#linkDiv a").eq(1).linkbutton('disable');
                $("#linkDiv a").eq(2).linkbutton('disable')
            }
        },
        columns : [ [ {
            field : "username",
            title : "账号",
            width : 100,
            align : "center"
        }, {
            field : "realname",
            title : "真实姓名",
            width : 100,
            align : "center"
        }, {
            field : "email",
            title : "邮箱",
            width : 100,
            align : "center"
        }, {
            field : "tel",
            title : "联系电话",
            width : 100,
            align : "center"
        }, {
            field : "inputtime",
            title : "录入时间",
            width : 100,
            align : "center"
        }, {
            field : "dept",
            title : "部门",
            width : 100,
            align : "center",
            formatter : deptFormatter
        }, {
            field : "admin",
            title : "管理员",
            width : 100,
            align : "center",
            formatter : adminFormatter
        },{
            field : "state",
            title : "状态",
            width : 100,
            align : "center",
            formatter : stateFormatter
        }, ] ]
    });

    selfPermissionDatagrid.datagrid({
        title:"自身权限",
        width:250,
        height:300,
        rownumbers:true,
        singleSelect:true,
        fitColumns:true,
        columns :[[{
            field : "name",
            title : "名字",
            width : 100,
            align : "center",
        }]]
    });

    allPermissionDatagrid.datagrid({
        url:"/permission_list",
        method:"post",
        title:"所有权限",
        width:250,
        height:300,
        pagination:true,
        rownumbers:true,
        singleSelect:true,
        fitColumns:true,
        columns :[[{
            field : "name",
            title : "名字",
            width : 100,
            align : "center",
        }]],
        //双击监听
        onDblClickRow:function (rowIndex,rowData) {
            var arraySelf = selfPermissionDatagrid.datagrid("getRows");
            console.info(rowData);
            //点击的权限，在自己权限列表中是否存在
            var flag = false;
            //在自己权限列表的index
            var index = -1;
            for(var i = 0;i < arraySelf.length; i++){
                if(arraySelf[i].id == rowData.id){
                    flag = true;
                    index = i;
                }
            }
            if(flag){
                selfPermissionDatagrid.datagrid("selectRow",index);
            }else{
                selfPermissionDatagrid.datagrid("appendRow",rowData);
            }
        }

    });
    //设置简单的分页
    var page = allPermissionDatagrid.datagrid("getPager");
    console.info(page);
    page.pagination({
        showPageList: false,
        showRefresh: false,
        displayMsg: ''
    });

    //role对话框
    roleDialog.dialog({
        width:600,
        height:500,
        draggable:false
    });
    //对话框居中
    roleDialog.dialog("center");

    //部门格式化
    function deptFormatter(dept, rowData, index) {
        if (dept) {
            return dept.name;
        }
        console.info(arguments);
    }

    //管理员格式化
    function adminFormatter(admin) {
        if (admin) {
            return "<font color='red'>管理员</font>";
        }
        return "普通人员"
    }

    //在职状态格式化
    function stateFormatter(state) {
        if(state){
            return "在职"
        }
        return "<font color='red'>离职</font>"
    }

    //方法管理
    var cmdObject = {
        add:function add() {
            roleDialog.dialog("open");
            roleDialog.dialog("setTitle", "新增");
            roleForm.form("clear");
        },
        delete:function del() {
            var selectData = roleDatagrid.datagrid("getSelected");
            //是否
            if (selectData) {
                $.messager.confirm('删除', '是否删除该数据', function (sure) {
                    if (sure) {
                        // 确定删除;
                        $.get("/employee_delete?id=" + selectData.id, function (data) {
                            if (data.success) {
                                $.messager.alert('提示', data.msg, 'info');
                                roleDatagrid.datagrid("reload");
                            } else {
                                $.messager.alert('提示', data.msg, 'error');
                            }
                        }, "json")
                    }
                });
            }else {
                $.messager.alert("提示", "你没有选择删除的数据啊", "info");
            }
        },
        edit:function edit() {
            var selectData = roleDatagrid.datagrid("getSelected");
            if (selectData) {
                roleDialog.dialog("open");
                roleDialog.dialog("setTitle", "编辑");
                roleForm.form("clear");
                //因为selectData没有这个属性dept.id，所以要添加上去
                if (selectData.dept) {
                    selectData["dept.id"] = selectData.dept.id;
                }
                //把数据写入到表格中
                roleForm.form("load", selectData);
                console.info(selectData);
            } else {
                $.messager.alert('提示', "你没有选择啊", 'info');
            }
        },
        search:function searchEmployee() {
            var parma ={};
            var dataArray = roleSearch.serializeArray();
            console.info(dataArray);
            for(var i = 0;i<dataArray.length;i++){
                parma[dataArray[i].name] = dataArray[i].value;
            }
            console.info(parma);
            roleDatagrid.datagrid("load",parma);
        },
        refresh:function refresh() {
            //当前页刷新
            roleDatagrid.datagrid("reload");
        },
        submit:function submit() {
            var id = $("#employee_dialog_form input[name='id']").val();
            var url;
            if (id) {
                url = "/employee_update";
            } else {
                url = "/employee_save";
            }

            roleForm.form("submit", {
                url: url,
                success: function (data) {
                    var respData = $.parseJSON(data);
                    if (respData.success) {
                        roleDialog.dialog("close");
                        $.messager.alert('提示', respData.msg, 'info', function () {
                            //按确定之后，重新请求数据
                            roleDatagrid.datagrid('load');
                        });
                    } else {
                        roleDialog.dialog("close");
                        $.messager.alert('提示', respData.msg, 'info');
                    }
                }
            });
        },
        cancel: function cancel() {
                roleDialog.dialog("close");
        }
    };

    $("a").bind("click",function () {
        var cmd = $(this).data("cmd");
        cmdObject[cmd]();
    });
});



