$(function () {
    var employeeDatagrid,employeeDialog,employeeForm,employeeSearch;
    employeeDatagrid = $("#datagrid");
    employeeDialog = $("#employee_dialog");
    employeeForm = $("#employee_dialog_form");
    employeeSearch = $("#employee_search_form");

    employeeDatagrid.datagrid({
        singleSelect:true,
        collapsible:true,
        url:'/employee_list',
        method:'post',
        pagination:true,
        pagePosition:'bottom',
        pageList: [3,5,10],
        pageSize:10,
        rownumbers:true,
        fit:true,
        fitColumns:true,
        onClickRow:function (rowIndex,rowData) {
            if(rowData.state){
                //eq 获取对应index的控件
                $("#linkDiv a").eq(1).linkbutton('enable');
                $("#linkDiv a").eq(2).linkbutton('enable')
            }else{
                $("#linkDiv a").eq(1).linkbutton('disable');
                $("#linkDiv a").eq(2).linkbutton('disable')
            }
        },
        columns : [ [ {
            field : "username",
            title : "账号",
            width : 100,
            align : "center"
        }, {
            field : "realname",
            title : "真实姓名",
            width : 100,
            align : "center"
        }, {
            field : "email",
            title : "邮箱",
            width : 100,
            align : "center"
        }, {
            field : "tel",
            title : "联系电话",
            width : 100,
            align : "center"
        }, {
            field : "inputtime",
            title : "录入时间",
            width : 100,
            align : "center"
        }, {
            field : "dept",
            title : "部门",
            width : 100,
            align : "center",
            formatter : deptFormatter
        }, {
            field : "admin",
            title : "管理员",
            width : 100,
            align : "center",
            formatter : adminFormatter
        },{
            field : "state",
            title : "状态",
            width : 100,
            align : "center",
            formatter : stateFormatter
        }, ] ]
    });

    //部门格式化
    function deptFormatter(dept, rowData, index) {
        if (dept) {
            return dept.name;
        }
        console.info(arguments);
    }

    //管理员格式化
    function adminFormatter(admin) {
        if (admin) {
            return "<font color='red'>管理员</font>";
        }
        return "普通人员"
    }

    //在职状态格式化
    function stateFormatter(state) {
        if(state){
            return "在职"
        }
        return "<font color='red'>离职</font>"
    }

    //方法管理
    var cmdObject = {
        add:function add() {
            employeeDialog.dialog("open");
            employeeDialog.dialog("setTitle", "新增");
            employeeForm.form("clear");
        },
        delete:function del() {
            var selectData = employeeDatagrid.datagrid("getSelected");
            //是否
            if (selectData) {
                $.messager.confirm('删除', '是否删除该数据', function (sure) {
                    if (sure) {
                        // 确定删除;
                        $.get("/employee_delete?id=" + selectData.id, function (data) {
                            if (data.success) {
                                $.messager.alert('提示', data.msg, 'info');
                                employeeDatagrid.datagrid("reload");
                            } else {
                                $.messager.alert('提示', data.msg, 'error');
                            }
                        }, "json")
                    }
                });
            }else {
                $.messager.alert('提示', "你没有选择删除的数据啊", 'info');
            }
        },
        edit:function edit() {
            var selectData = employeeDatagrid.datagrid("getSelected");
            if (selectData) {
                employeeDialog.dialog("open");
                employeeDialog.dialog("setTitle", "编辑");
                employeeForm.form("clear");
                //因为selectData没有这个属性dept.id，所以要添加上去
                if (selectData.dept) {
                    selectData["dept.id"] = selectData.dept.id;
                }
                //把数据写入到表格中
                $('#employee_dialog_form').form("load", selectData);
                console.info(selectData);
            } else {
                $.messager.alert('提示', "你没有选择啊", 'info');
            }
        },
        search:function searchEmployee() {
            var parma ={};
            var dataArray = employeeSearch.serializeArray();
            console.info(dataArray);
            for(var i = 0;i<dataArray.length;i++){
                parma[dataArray[i].name] = dataArray[i].value;
            }
            console.info(parma);
            employeeDatagrid.datagrid("load",parma);
        },
        refresh:function refresh() {
            //当前页刷新
            employeeDatagrid.datagrid("reload");
        },
        submit:function submit() {
            var id = $("#employee_dialog_form input[name='id']").val();
            var url;
            if (id) {
                url = "/employee_update";
            } else {
                url = "/employee_save";
            }

            employeeForm.form("submit", {
                url: url,
                success: function (data) {
                    var respData = $.parseJSON(data);
                    if (respData.success) {
                        employeeDialog.dialog("close");
                        $.messager.alert('提示', respData.msg, 'info', function () {
                            //按确定之后，重新请求数据
                            employeeDatagrid.datagrid('load');
                        });
                    } else {
                        employeeDialog.dialog("close");
                        $.messager.alert('提示', respData.msg, 'info');
                    }
                }
            });
        },
        cancel: function cancel() {
                employeeDialog.dialog("close");
        }
    };

    $("a").bind("click",function () {
        var cmd = $(this).data("cmd");
        cmdObject[cmd]();
    });
});



