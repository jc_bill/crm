<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="/js/linkHead.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>小码哥客户关系管理系统</title>
    <link rel="stylesheet" href="/css/style.css">
    <script type="text/javascript">
        $(function () {
            $("#loginBtn").click(function () {
                $.post("/login",$("form").serialize(),function (data) {
                    console.info(data);
                    if(data.success){
                        window.location.href = "/main"
                    }else {
                        $.messager.alert('提示',data.msg,'error');
                    }
                },"json");
            });
            
            $("#resetBtn").click(function () {
                $("form")[0].reset();
            })
        });
    </script>
</head>
<body>
<section class="container">
    <div class="login">
        <h1>用户登录</h1>
        <form id="myform"  method="post" enctype="application/x-www-form-urlencoded">
            <p><input type="text" name="username" value="" placeholder="账号"></p>
            <p><input type="password" name="password" value="" placeholder="密码"></p>
            <p class="submit">
                <input id="loginBtn" type="button" value="登录">
                <input id="resetBtn" type="button" value="重置">
            </p>
        </form>
    </div>
</section>
<div style="text-align:center;" class="login-help">
    <p>Copyright ©2015 广州小码哥教育科技有限公司</p>
</div>
</html>