package com.cheng.crm.service.impl;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.page.PageResult;
import com.cheng.crm.query.EmployeeQueryObject;
import com.cheng.crm.service.IEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:ApplicationContext.xml")
@SuppressWarnings("all")
public class EmployeeServiceTest {

    @Autowired
    private IEmployeeService service;

    @Test
    public void save() throws Exception {
        Employee employee = new Employee();
        employee.setUsername("123");
        service.save(employee);
    }

    @Test
    public void delete() throws Exception {
    }

    @Test
    public void update() throws Exception {
    }

    @Test
    public void getCount() throws Exception {
        EmployeeQueryObject queryObject = new EmployeeQueryObject();
        queryObject.setKeyword("1");
        service.getCount(queryObject);
    }

    @Test
    public void getListByCondition() throws Exception {
//        EmployeeQueryObject queryObject = new EmployeeQueryObject();
//        queryObject.setPage(1L);
//        queryObject.setRows(10L);
//        queryObject.setStartDate(new Date());
////        queryObject.setState(true);
//        System.out.println("listByCondition.getRows() = " + listByCondition.getRows());
        
    }

}