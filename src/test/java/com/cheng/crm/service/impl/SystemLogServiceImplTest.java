package com.cheng.crm.service.impl;

import com.cheng.crm.domain.Employee;
import com.cheng.crm.domain.SystemLog;
import com.cheng.crm.service.ISystemLogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:ApplicationContext.xml")
public class SystemLogServiceImplTest {

    @Autowired
    private ISystemLogService service;

    @Test
    public void save() throws Exception {
        Employee user = new Employee();
        user.setUsername("晓勇哥");

        SystemLog systemLog = new SystemLog();
        systemLog.setOptime(new Date());
        systemLog.setOpip("sdfs");
        systemLog.setUser(user);
        service.save(systemLog);
    }

}